﻿namespace ConsoleApp1
{
    public class FurnitureFactory: IFactory
    {
        public ISteel CreateSteel()
        {
            return new FurnitureProductSteel();
        }

        public IWood CreateWood()
        {
            return new FurnitureProductWood();
        }
    }
}