﻿namespace ConsoleApp1
{
    public class FurnitureProductSteel: ISteel
    {
        private string _steelType;
        
        public string GetSteelType()
        {
            return _steelType;
        }

        public void SetSteelType(string steelType)
        {
            _steelType = steelType;
        }
    }
}