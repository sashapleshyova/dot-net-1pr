﻿namespace ConsoleApp1
{
    public class Client
    {
        private readonly IWood _wood;
        private readonly ISteel _steel;

        public Client(IFactory factory)
        {
            _wood = factory.CreateWood();
            _steel = factory.CreateSteel();
        }

        public IWood GetWood()
        {
            return _wood;
        }

        public ISteel GetSteel()
        {
            return _steel;
        }
    }
}