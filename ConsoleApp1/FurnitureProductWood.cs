﻿namespace ConsoleApp1
{
    public class FurnitureProductWood: IWood
    {
        private string _woodType;

        public string GetWoodType()
        {
            return _woodType;
        }

        public void SetWoodType(string woodType)
        {
            _woodType = woodType;
        }
    }
}