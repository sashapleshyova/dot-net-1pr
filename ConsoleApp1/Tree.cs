﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1
{
     public class Tree<T> : IEnumerable<T> where T: IComparable<T>
    {
        private Tree<T> parent, left, rigth;
        private T value;
        private readonly IComparable<T> _comparator;

        public Tree(T value, Tree<T> parent)
        {
            this.value = value;
            this.parent = parent;
        }

        public Tree(T value, Tree<T> parent, IComparable<T> comparator)
        {
            this.value = value;
            this.parent = parent;
            _comparator = comparator;
        }

        public Tree(IEnumerable<T> collection)
        {
            if (collection is ICollection<T> c)
            {
                var count = c.Count;

                if (count == 0) return;
                
                foreach (var comparable in c)
                {
                    Add(comparable);
                }
            }
            else
            {
                using var en = collection!.GetEnumerator();
                while (en.MoveNext())
                {
                    Add(en.Current);
                }
            }
        }

        public void Add(T val)
        {
            var comp = _comparator ?? val;
            
            if (comp.CompareTo(value) > 0)
            {
                if (left == null)
                {
                    left = new Tree<T>(val, this, _comparator);
                }
                else
                {
                    left?.Add(val);
                }
            }
            else
            {
                if (rigth == null)
                {
                    rigth = new Tree<T>(val, this, _comparator);
                }
                else
                {
                    rigth?.Add(val);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (left != null)
            {
                foreach(var val in left)
                {
                    yield return val;
                }
            }

            yield return value;

            if (rigth == null) yield break;
            {
                foreach (var val in rigth)
                {
                    yield return val;
                }
            }
        }
        
        public IEnumerator<T> GetReversedEnumerator()
        {
            if (parent == null) yield break;
 
            var stack = new Stack<Tree<T>>();
            var node = parent;
 
            while (stack.Count > 0 || node != null) {
                if (node == null) {
                    node = stack.Pop();
                    if (stack.Count > 0 && node.rigth == stack.Peek()) {
                        stack.Pop();
                        stack.Push(node);
                        node = node.rigth;
                    }
                    else { yield return node.value; node = null; }
                }
                else {
                    if (node.rigth != null) stack.Push(node.rigth);
                    stack.Push(node);
                    node = node.left;
                }
            }
        }

        public Tree<T> FindByValue(T val)
        {
            return FindByValue(this, val);
        }

        private static Tree<T> FindByValue(Tree<T> tree, T value)
        {
            if(tree == null) return null;
            
            return value.CompareTo(tree.value) switch
            {
                1 => FindByValue(tree.rigth, value),
                -1 => FindByValue(tree.left, value),
                0 => tree,
                _ => null
            };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }
}