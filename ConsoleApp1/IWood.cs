﻿namespace ConsoleApp1
{
    public interface IWood
    {
        string GetWoodType();
        void SetWoodType(string woodType);
    }
}