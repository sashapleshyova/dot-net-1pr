﻿namespace ConsoleApp1
{
    public class Student
    {
        private readonly string _name;
        private readonly string _testName;
        private readonly string _testDate;
        private readonly int _testScore;

        public Student(string name, string testName, string testDate, int testScore)
        {
            _name = name;
            _testName = testName;
            _testDate = testDate;
            _testScore = testScore;
        }

        public int getTestScore()
        {
            return _testScore;
        }

        public override string ToString()
        {
            return _name + "\n" + _testName + "\n" + _testDate + "\n" + _testScore + "\n";
        }
    }
}