﻿namespace ConsoleApp1
{
    public interface ISteel
    {
        string GetSteelType();
        void SetSteelType(string steelType);
    }
}