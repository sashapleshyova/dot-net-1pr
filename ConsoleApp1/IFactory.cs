﻿namespace ConsoleApp1
{
    public interface IFactory
    {
        public ISteel CreateSteel();

        public IWood CreateWood();
    }
}