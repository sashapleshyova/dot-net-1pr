﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1
{
    
    class Program
    {
        static void Main(string[] args)
        {
            // arrayToTree();
            // studentsTest();
            secondTask();
        }

        static void arrayToTree()
        {
            int[] array = {7, 10,15,65,0,-4,0,-6};
            
            var tree  = new Tree<int>(array[0], null);

            for (var i = 1; i < array.Length; i++)
            {
                tree.Add(array[i]);
            }
            
            using var en = tree.GetEnumerator();
            
            while (en.MoveNext())
            {
                Console.Write(en.Current +" ");
            }
        }

        static void studentsTest()
        {
            var studentsList = new List<Student>();

            for (var i = 0; i < 3; i++)
            {
                Console.WriteLine("\n Имя студента: ");
                var name = Console.ReadLine();
                
                Console.WriteLine("Название теста: ");
                var testName = Console.ReadLine();
                
                Console.WriteLine("Дата: ");
                var testDate = Console.ReadLine();
                
                Console.WriteLine("Баллы: ");
                var testScore = Convert.ToInt32(Console.ReadLine());
                
                studentsList.Add(new Student(name, testName, testDate, testScore));
            }
            
            var testResults = new Tree<int>(studentsList[0].getTestScore(), null);

            for (var i = 1; i < studentsList.Count; i++)
            {
                testResults.Add(studentsList[i].getTestScore());
            }
            
            using var en = testResults.GetEnumerator();
            
            while (en.MoveNext())
            {
                Console.Write(en.Current + " ");
            }
        }

        static void secondTask()
        {
            var factory = new FurnitureFactory();
            var client = new Client(factory);
            client.GetSteel().SetSteelType("A1");
            client.GetWood().SetWoodType("oak");
            
            Console.WriteLine("Steel type: " + client.GetSteel().GetSteelType());
            Console.WriteLine("Wood type: " + client.GetWood().GetWoodType());
        }
    }
}